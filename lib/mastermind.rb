class Code

  attr_reader :pegs

  PEGS = {"R" => "red", "G" => "green", "B" => "blue",
          "Y" => "yellow", "O" => "orange", "P" => "purple"}

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(str)
    sequence = str.upcase.chars
    sequence.each do |char|
      raise "Invalid Peg" unless PEGS.has_key?(char)
    end
    Code.new(sequence)
  end

  def self.random
    Code.new(PEGS.keys.sample(4))
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(other_code)
    count = 0

    @pegs.each_with_index do |peg, idx|
      if @pegs[idx] == other_code[idx]
        count += 1
      end
    end

    count
  end

  def near_matches(other_code)
    count = 0
    other_pegs = Hash.new(0)

    other_code.pegs.each do |peg|
      other_pegs[peg] += 1
    end

    @pegs.each do |peg|
      if other_pegs.keys.include?(peg) && other_pegs[peg] > 0
        count += 1
        other_pegs[peg] -= 1
      end
    end

    count - self.exact_matches(other_code)
  end

  def ==(other_code)
    return false unless other_code.instance_of? Code
    self.exact_matches(other_code) == 4
  end

end


class Game

  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
    @guess_count = 0
  end

  def get_guess
    puts "Guess the code: "
    @guess_count += 1
    Code.parse(gets.chomp)
  end

  def display_matches(other_code)
    exact = @secret_code.exact_matches(other_code)
    near = @secret_code.near_matches(other_code)
    puts "#{exact} exact matches"
    puts "#{near} near matches"
  end

  def play
    while @guess_count < 10
      guess = self.get_guess
      if guess == @secret_code
        puts "You win!"
        return
      else
        puts "Try again"
        self.display_matches(guess)
      end
    end

    puts "You lost."
  end

end

if $PROGRAM_NAME == __FILE__
  Game.new.play
end
